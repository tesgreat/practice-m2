== Доступ к приложению внутри кластера. Service для company-service и location-service

Для выполнения задания необходимо запустить `minikube` с работающей БД из `exercise-1`.

А также для `company-service` и `location-service`:

* `ConfigMap`
* `Secret`
* `Deployment`
** `image` : `registry.gitlab.com/cloud-native-development1/module-2/company-service:<имя тэга>-0.0.4`
** `image` : `registry.gitlab.com/cloud-native-development1/module-2/location-service:<имя тэга>-0.0.4`


=== Company-service

. Создайте манифест `Service` для `Deployment` из `exercise-6`
.. `name` : `company-api`
.. Используйте `selector` для `company-service`: `app: company` и `tier: backend` и `type: api`
.. `type` : `ClusterIP`
.. `spec.ports[0].port`  = `80` - порт по которому будет доступен сервис
.. `spec.ports[0].targetPort` = `8080` (или имя заданное в `spec.template.spec.containers[0].ports[0].name` для `deployments/company-api`)
.. `spec.ports[0].name=http` дать имя порту для последующего использования
. Создайте ресурс на основе манифеста
. Обновите конфигурацию через `ConfigMap`
.. Добавьте property - `location.uri=http://location-api`, где `http://location-api` dns имя Service `location-service` внутри k8s
. Примените обновления для `ConfigMap`
. Перезапустите Pod `company-api` (удалите существующий), для того чтобы новая конфигурация применилась


=== Location-service

. Создайте манифест `Service` для `Deployment` из `exercise-6`
.. `name` : `location-api`
.. Используйте `selector` для `location-service`: `app: location` и `tier: backend` и `type: api`
.. `type` : `ClusterIP`
.. `spec.ports[0].port` = `80` - порт по которому будет доступен сервис
.. `spec.ports[0].targetPort` = `8081` (или имя заданное в `spec.template.spec.containers[0].ports[0].name` для `deployments/company-api`)
.. `spec.ports[0].name=http` дать имя порту для последующего использования
. Создайте ресурс на основе манифеста
. Обновите конфигурацию через `ConfigMap`
.. Добавьте property - `company.uri=http://company-api`, где `http://company-api` dns имя Service `company-service` внутри k8s
. Примените обновления для `ConfigMap`
. Перезапустите Pod `location-api` (удалите существующий), для того чтобы новая конфигурация применилась

=== Проверка работоспособности

. Создайте NodePort service для `company-service`
+
[source, bash]
----
kubectl expose deployment company-api --name=company-node-port --type=NodePort
----
. Создайте NodePort service для `location-service`
+
[source, bash]
----
kubectl expose deployment location-api --name=location-node-port --type=NodePort
----

. Проверьте что сервисы взаимодействуют друг с другом.
.. Найдите адрес NodePort `company-service` сервиса:
+
[source, bash]
----
echo "http://$(minikube ip):$(kubectl get svc company-node-port -o=jsonpath='{.spec.ports[?(@.port==8080)].nodePort}')/api/companies/1/locations"
----
.. Найдите адрес NodePort `location-service` сервиса:
+
[source, bash]
----
echo "http://$(minikube ip):$(kubectl get svc company-node-port -o=jsonpath='{.spec.ports[?(@.port==8081)].nodePort}')/api/companies/1/locations"
----
.. сделайте запрос на `<адрес из предыдущего пункта>/api/companies/1/locations`
.. сделайте запрос на `<адрес из предыдущего пункта>/api/locations/5/company`


https://kubernetes.io/docs/concepts/services-networking/service/[Service^]

https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/[Использование Service для коммуникации приложений внутри кластера.^]

https://kubernetes.io/docs/reference/kubectl/cheatsheet/[kubectl cheat sheet.^]
