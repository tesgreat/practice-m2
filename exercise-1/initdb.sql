create schema company;

CREATE ROLE company WITH
    LOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    INHERIT
    NOREPLICATION
    CONNECTION LIMIT -1
    PASSWORD '';
COMMENT ON ROLE company IS 'user for company service';

grant connect on database cnad to company;
grant usage on schema company to company;
grant create on schema company to company;

create schema location;

CREATE ROLE location WITH
    LOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    INHERIT
    NOREPLICATION
    CONNECTION LIMIT -1
    PASSWORD '';
COMMENT ON ROLE location IS 'user for location service';

grant connect on database cnad to location;
grant usage on schema location to location;
grant create on schema location to location;
