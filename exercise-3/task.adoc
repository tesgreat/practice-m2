== Контейнеризация Company-service и location-service

Для выполнения задания необходимо запустить `minikube` с работающей БД из `exercise-1`.
Также перед сборкой контейнера приложение необходимо собрать:
[source, bash]
----
mvn clean package
----

Тренер раздаст ключи для возможности читать и писать в приватный docker registry.

. Склонируйте или сделайте форк `сompany-service`
+
https://gitlab.com/cloud-native-development1/module-2/company-service[^]
+
[source, bash]
----
git clone https://gitlab.com/cloud-native-development1/module-2/company-service.git
----
+
или
+
[source, bash]
----
cd company-service
git pull
----
. Создайте `Dockerfile`
.. используйте минимальный базовый образ на свой выбор (лучше на базе Alpine Linux, JRE)
.. LTS 11 (можно и другую)
.. Создайте пользователя и группу для запуска не от root пользователя
+
.Alpine based images
[source, bash]
----
addgroup -g 1001 --system java && adduser -u 1001 --system java --g 1001
----
+
.debian based images
[source, bash]
----
addgroup --gid 1001 --system java && adduser -u 1001 --system java --gid 1001
----

.. скопируйте артефакт приложения в контейнер
.. определите команду вызова (лучше использовать exec форму)
. Соберите контейнер
+
[source, bash]
----
docker build -t registry.gitlab.com/cloud-native-development1/module-2/company-service:<tag-name> .  <1> <2>
----
<1> [tag-name] используйте свое имя латиницей kebab-case (Aliaksandr Nozdryn-Platnitski a-nozdryn-platnitski).
<2> не забудьте `.` в конце команды, она указывает контекст текущая директория.

. Запустите контейнер на основе образа и проверьте, что приложение работает и открывается.
. Используя Deploy Token опубликуйте ваш образ `registry.gitlab.com/cloud-native-development1/module-2/company-service` репозитории.
. Зайдите на https://gitlab.com/cloud-native-development1/module-2/company-service/container_registry[страничку registry^] и убедитесь, что образ опубликовался
+
image::company-service-registry.png[]

=== Домашнее задание

. Склонируйте или сделайте форк `location-service`
+
https://gitlab.com/cloud-native-development1/module-2/location-service[^]
+
[source, bash]
----
git clone https://gitlab.com/cloud-native-development1/module-2/location-service.git
----
+
или
+
[source, bash]
----
cd location-service
git pull
----

. Создайте `Dockerfile`
.. используйте минимальный базовый образ на свой выбор (лучше на базе Alpine Linux, JRE)
.. LTS 11 (можно и другую)
.. Создайте пользователя и группу для запуска не от root пользователя
+
.Alpine based images
[source, bash]
----
addgroup -g 1001 --system java && adduser -u 1001 --system java --g 1001
----
+
.debian based images
[source, bash]
----
addgroup --gid 1001 --system java && adduser -u 1001 --system java --gid 1001
----

.. скопируйте артефакт приложения в контейнер
.. определите команду вызова (лучше использовать exec форму)
. Соберите контейнер
+
[source, bash]
----
docker build -t registry.gitlab.com/cloud-native-development1/module-2/location-service:<tag-name> .  <1> <2>
----
<1> [tag-name] используйте свое имя латиницей kebab-case (Aliaksandr Nozdryn-Platnitski a-nozdryn-platnitski).
<2> не забудьте `.` в конце команды, она указывает контекст текущая директория.

. Запустите контейнер на основе образа и проверьте, что приложение работает и открывается.
. Используя Deploy Token опубликуйте ваш образ `registry.gitlab.com/cloud-native-development1/module-2/location-service` репозитории.
. Зайдите на https://gitlab.com/cloud-native-development1/module-2/location-service/container_registry[страничку registry^] и убедитесь, что образ опубликовался
+
image::location-service-registry.png[]



https://docs.docker.com/engine/reference/builder/[Dockerfile инструкции.^]
